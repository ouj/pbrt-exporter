#!/usr/local/bin/python

import sys
import os
import glob
import subprocess

usage_msg = "Usage: python export_pbrt.py <input file>.pbrt <output file>.[json|nson]"

def find_exporter(script_path):
    (script_folder, script_name) = os.path.split(script_path)
    exporter = os.path.abspath(os.path.join(script_folder, "bin", "Release", "pbrtexport"))
    
    if not os.path.exists(exporter) and not os.path.exists(exporter+".exe"):
        exporter = os.path.abspath(os.path.join(script_folder, "bin", "Debug", "pbrtexport"))
    if not os.path.exists(exporter) and not os.path.exists(exporter+".exe"):
        return None
    
    return exporter

def convert_images(texture_folder):
    os.chdir(texture_folder)
    ext_hdr = [".[Ee][Xx][Rr]", ".[Tt][Gg][Aa]"]
    image_hdr = []
    for ext in ext_hdr:
        img_path = os.path.normpath("*" + ext)
        gp = glob.glob(img_path)
        image_hdr += gp

    for fname in image_hdr:
        if os.path.isfile(fname):
            (base, extension) = os.path.splitext(fname)
            nfname = base + ".pfm"
            cmd = ["hdrconv", fname, nfname]
            subprocess.call(cmd)
            os.remove(fname)
    
    
def main(argv=None):
    if argv is None:
        argv = sys.argv
    if len(argv) < 3:
        print (usage_msg)
        return
        
    exporter = find_exporter(argv[0])
    
    input_file = os.path.abspath(argv[1])
    output_file = os.path.abspath(argv[2])
    
    (input_folder, input_filename) = os.path.split(input_file)
    (output_folder, output_filename) = os.path.split(output_file)
    (data_folder, extension) = os.path.splitext(output_filename)
    
    if extension != ".jbin" and extension != '.json':
        print ("incorrect output file extension")
        print (usage_msg)
        return
        
    geometry_folder = os.path.join(output_folder, data_folder, "geometry")
    texture_folder = os.path.join(output_folder, data_folder, "texture")
    
    if not os.path.exists(geometry_folder):
        os.makedirs(geometry_folder)
    if not os.path.exists(texture_folder):
        os.makedirs(texture_folder)
    
    os.chdir(input_folder)
    #print "switched to:", os.getcwd()
    
    # Export
    p = subprocess.Popen([exporter, input_filename, output_filename, output_folder])
    p.wait()
    
    # Image Process
    convert_images(texture_folder)
    
    
if __name__ == "__main__":
    sys.exit(main())
