
/*
    pbrt source code Copyright(c) 1998-2010 Matt Pharr and Greg Humphreys.

    This file is part of pbrt.

    pbrt is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.  Note that the text contents of
    the book "Physically Based Rendering" are *not* licensed under the
    GNU GPL.

    pbrt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */


// main/pbrt.cpp*
#include "stdafx.h"
#include "api.h"
#include "probes.h"
#include "parser.h"
#include "parallel.h"
#include <common/debug.h>
#include "exporter/exporter.h"

// main program
int main(int argc, char *argv[]) {
    pbrt::Options options;
    string infile;
    string outfile;
    string folder;
    // Process command-line arguments
    if (argc < 4) {
        printf("usage: pbrtexport [input_filename] [output_filename] [ouput_folder]\n");
    }
    infile = argv[1];
    outfile = argv[2];
    folder = argv[3];

    printf("be advised, in this program, we memory leak in purpose to keep code simple.\n");
    pbrt::pbrtInit(options);
    exporter::init(outfile, folder);
    // Process scene description
    PBRT_STARTED_PARSING();

    options.imageFile = outfile;
    if (!ParseFile(infile))
        printf("couldn't open scene file \"%s\"\n", infile.c_str());

    exporter::exportScene();
    exporter::cleanup();
    pbrt::pbrtCleanup();
    return 0;
}


