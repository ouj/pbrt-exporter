
/*
    pbrt source code Copyright(c) 1998-2010 Matt Pharr and Greg Humphreys.

    This file is part of pbrt.

    pbrt is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.  Note that the text contents of
    the book "Physically Based Rendering" are *not* licensed under the
    GNU GPL.

    pbrt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */


// materials/uber.cpp*
#include "stdafx.h"
#include "materials/uber.h"
#include "spectrum.h"
#include "texture.h"
#include "paramset.h"

namespace pbrt {

UberMaterial *CreateUberMaterial(const Transform &xform,
        const TextureParams &mp) {
            return 0;
    //Reference<Texture<Spectrum> > Kd = mp.GetSpectrumTexture("Kd", Spectrum(0.25f));
    //Reference<Texture<Spectrum> > Ks = mp.GetSpectrumTexture("Ks", Spectrum(0.25f));
    //Reference<Texture<Spectrum> > Kr = mp.GetSpectrumTexture("Kr", Spectrum(0.f));
    //Reference<Texture<float> > roughness = mp.GetFloatTexture("roughness", .1f);
    //Reference<Texture<float> > eta = mp.GetFloatTexture("index", 1.5f);
    //Reference<Texture<Spectrum> > opacity = mp.GetSpectrumTexture("opacity", 1.f);
    //Reference<Texture<float> > bumpMap = mp.GetFloatTexture("bumpmap", 0.f);
    //return new UberMaterial(Kd, Ks, Kr, roughness, opacity, eta, bumpMap);
}


}