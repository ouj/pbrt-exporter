
/*
    pbrt source code Copyright(c) 1998-2010 Matt Pharr and Greg Humphreys.

    This file is part of pbrt.

    pbrt is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.  Note that the text contents of
    the book "Physically Based Rendering" are *not* licensed under the
    GNU GPL.

    pbrt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef PBRT_LIGHTS_DISTANT_H
#define PBRT_LIGHTS_DISTANT_H

// lights/distant.h*
#include "pbrt.h"
#include "light.h"
#include "shape.h"
#include "scene.h"
namespace pbrt {
// DistantLight Declarations
class DistantLight : public Light {
public:
    // DistantLight Public Methods
    DistantLight(const Transform &light2world, const Spectrum &radiance, const Vector &dir);
private:
    // DistantLight Private Data
    Vector lightDir;
    Spectrum L;
};


DistantLight *CreateDistantLight(const Transform &light2world,
        const ParamSet &paramSet);
}
#endif // PBRT_LIGHTS_DISTANT_H
