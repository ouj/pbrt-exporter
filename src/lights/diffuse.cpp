
/*
    pbrt source code Copyright(c) 1998-2010 Matt Pharr and Greg Humphreys.

    This file is part of pbrt.

    pbrt is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.  Note that the text contents of
    the book "Physically Based Rendering" are *not* licensed under the
    GNU GPL.

    pbrt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */


// lights/diffuse.cpp*
#include "stdafx.h"
#include "lights/diffuse.h"
#include "paramset.h"
#include "montecarlo.h"

namespace pbrt {
    
// DiffuseAreaLight Method Definitions
DiffuseAreaLight::~DiffuseAreaLight() {
    delete shapeSet;
}


DiffuseAreaLight::DiffuseAreaLight(const Transform &light2world,
        const Spectrum &le, int ns, const Reference<Shape> &s)
    : AreaLight(light2world, ns) {
    Lemit = le;
    shapeSet = new ShapeSet(s);
    area = shapeSet->Area();
}



//AreaLight *CreateDiffuseAreaLight(const Transform &light2world, const ParamSet &paramSet, const Shape *shape) {
//    Spectrum L = paramSet.FindOneSpectrum("L", Spectrum(1.0));
//    Spectrum sc = paramSet.FindOneSpectrum("scale", Spectrum(1.0));
//    int nSamples = paramSet.FindOneInt("nsamples", 1);
//    return new DiffuseAreaLight(light2world, L * sc, nSamples, shape);
//}


}

