
/*
pbrt source code Copyright(c) 1998-2010 Matt Pharr and Greg Humphreys.

This file is part of pbrt.

pbrt is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.  Note that the text contents of
the book "Physically Based Rendering" are *not* licensed under the
GNU GPL.

pbrt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


// core/parallel.cpp*
#include "stdafx.h"
#include "parallel.h"
#include "memory.h"
#ifdef PBRT_USE_GRAND_CENTRAL_DISPATCH
#include <dispatch/dispatch.h>
#endif // PBRT_USE_GRAND_CENTRAL_DISPATCH
#if !defined(PBRT_IS_WINDOWS)
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/sysctl.h>
#include <errno.h>
#endif 
#include <list>



Mutex *Mutex::Create() {
    return new Mutex;
}



void Mutex::Destroy(Mutex *m) {
    delete m;
}



Mutex::Mutex() {
}



Mutex::~Mutex() {
}



MutexLock::MutexLock(Mutex &m) : mutex(m) {
}



MutexLock::~MutexLock() {
}



RWMutex *RWMutex::Create() {
    return new RWMutex;
}



void RWMutex::Destroy(RWMutex *m) {
    delete m;
}



// vista has 'slim reader/writer (SRW)' locks... sigh.

RWMutex::RWMutex() {

}



RWMutex::~RWMutex() {

}




RWMutexLock::RWMutexLock(RWMutex &m, RWMutexLockType t)
    : type(t), mutex(m) {
}



RWMutexLock::~RWMutexLock() {
}



void RWMutexLock::UpgradeToWrite() {
}



void RWMutexLock::DowngradeToRead() {
}


Semaphore::Semaphore() {
}

Semaphore::~Semaphore() {
}



void Semaphore::Wait() {
}


bool Semaphore::TryWait() {
    return true;
}


void Semaphore::Post(int count) {
}


ConditionVariable::ConditionVariable() {
}


ConditionVariable::~ConditionVariable() {
}

void ConditionVariable::Lock() {
}

void ConditionVariable::Unlock() {
}

void ConditionVariable::Wait() {
}

void ConditionVariable::Signal() {
}

void TasksInit() {
    return;
}


void TasksCleanup() {
    return;
}


Task::~Task() {
}


void EnqueueTasks(const vector<Task *> &tasks) {
    for (unsigned int i = 0; i < tasks.size(); ++i)
        tasks[i]->Run();
    return;
}


void WaitForAllTasks() {
    return;
}


int NumSystemCores() {
    return 1;
}


