
/*
    pbrt source code Copyright(c) 1998-2010 Matt Pharr and Greg Humphreys.

    This file is part of pbrt.

    pbrt is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.  Note that the text contents of
    the book "Physically Based Rendering" are *not* licensed under the
    GNU GPL.

    pbrt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */


// core/montecarlo.cpp*
#include "stdafx.h"
#include "montecarlo.h"
#include "geometry.h"
#include "shape.h"
#include "volume.h"

namespace pbrt {


Distribution2D::Distribution2D(const float *func, int nu, int nv) {
    pConditionalV.reserve(nv);
    for (int v = 0; v < nv; ++v) {
        // Compute conditional sampling distribution for $\tilde{v}$
        pConditionalV.push_back(new Distribution1D(&func[v*nu], nu));
    }
    // Compute marginal sampling distribution $p[\tilde{v}]$
    vector<float> marginalFunc;
    marginalFunc.reserve(nv);
    for (int v = 0; v < nv; ++v)
        marginalFunc.push_back(pConditionalV[v]->funcInt);
    pMarginal = new Distribution1D(&marginalFunc[0], nv);
}


Distribution2D::~Distribution2D() {
    delete pMarginal;
    for (uint32_t i = 0; i < pConditionalV.size(); ++i)
        delete pConditionalV[i];
}

}

