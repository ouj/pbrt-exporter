
/*
pbrt source code Copyright(c) 1998-2010 Matt Pharr and Greg Humphreys.

This file is part of pbrt.

pbrt is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.  Note that the text contents of
the book "Physically Based Rendering" are *not* licensed under the
GNU GPL.

pbrt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef PBRT_CORE_FILM_H
#define PBRT_CORE_FILM_H

// core/film.h*
#include "pbrt.h"
#include "filter.h"
#include "paramset.h"

namespace pbrt {

    // Film Declarations
    class Film {
    public:
        // Film Interface
        Film(int xres, int yres)
            : xResolution(xres), yResolution(yres) { }
        virtual ~Film() {};

        // Film Public Data
        const int xResolution, yResolution;
    };

    inline Film *CreateFilm(const ParamSet &params) {
        string filename = params.FindOneString("filename", "");
        int xres = params.FindOneInt("xresolution", 640);
        int yres = params.FindOneInt("yresolution", 480);
        float crop[4] = { 0, 1, 0, 1 };
        int cwi;
        const float *cr = params.FindFloat("cropwindow", &cwi);
        if (cr && cwi == 4) {
            crop[0] = Clamp(min(cr[0], cr[1]), 0., 1.);
            crop[1] = Clamp(max(cr[0], cr[1]), 0., 1.);
            crop[2] = Clamp(min(cr[2], cr[3]), 0., 1.);
            crop[3] = Clamp(max(cr[2], cr[3]), 0., 1.);
        }
        return new Film(xres, yres);
    }

}

#endif // PBRT_CORE_FILM_H
