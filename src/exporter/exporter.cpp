#include "exporter.h"
#include "maker.h"
#include "paramset.h"
#include <scene/shape.h>
#include <iostream>
#include "geometry.h"
#include <scene/reflectance.h>
#include "export_scene.h"
#include <scene/light.h>
#include <scene/camera.h>
#include <common/raw.h>
#include <common/stddir.h>
#include <common/binio.h>
#include "convert.h"
#include "utils.h"
#include "namer.h"
using std::cout;
using std::endl;

namespace exporter{
    static ExportScene *scene;
    static string filepath;
    static string geometry_folder;
    static string texture_folder;
    static string geometry_folder_path;
    static string texture_folder_path;

#ifdef WIN32
    static const char div = '\\';
#else
    static const char div = '/';
#endif // WIN32

    void init(const string &outfile, const string &folderpath) {
        // make directories and set folder path.
        string foldername = outfile.substr(0, outfile.size() - 5);

        geometry_folder = foldername + "/geometry/";
        texture_folder = foldername + "/texture/";

        filepath = folderpath + div + outfile;

        string data_folder = folderpath + div + foldername;
        geometry_folder_path = data_folder + div + "geometry" + div;
        texture_folder_path = data_folder + div + "texture" + div;

        cout << "output file: " << outfile << endl;
        cout << "in directory: " << folderpath << endl;
        cout << "path: " << filepath << endl;
        cout << "geometry folder: " << geometry_folder << endl;
        cout << "texture folder: " << texture_folder << endl;

        scene = new ExportScene();
    }

    template<typename T>
    jsonArray procSceneObjectMap(const std::map<std::string, T*>& val, jsonObject (*procFunc)(const T*)) {
        jsonArray json(val.size());
        typename std::map<std::string, T*>::const_iterator it;
        int i = 0;
        for(it = val.begin(); it != val.end(); it++) {
            json[i] = procFunc(it->second);
            i++;
        }
        return json;
    }

    template<typename A, typename R>
    inline jsonArray _impl_procNumberArray(const A& array, size_t nc) {
        jsonArray json(array.size()*nc);
        const R* ptr = (const R*)(&(array[0]));
        for(size_t i = 0; i < json.size(); i ++) json[i] = ptr[i];
        return json;
    }

    jsonArray procVec2fArray(const std::vector<vec2f>& array) { return _impl_procNumberArray<std::vector<vec2f>,float>(array,2); }
    jsonArray procVec3fArray(const std::vector<vec3f>& array) { return _impl_procNumberArray<std::vector<vec3f>,float>(array,3); }
    jsonArray procVec3iArray(const std::vector<vec3i>& array) { return _impl_procNumberArray<std::vector<vec3i>,int>(array,3); }


    jsonObject procTexture(const ::Texture *val) {
        const PbrtTexture* tex = static_cast<const PbrtTexture*>(val);
        error_if_not(tex != 0, "invalid texture");
        jsonObject json;
        json["type"] = "texture";
        json["name"] = tex->name;
        json["wrap"] = "repeat";
        if (tex->wrap == Texture::REPEAT) {
            json["wrap"] = "repeat";
        } else if (tex->wrap == Texture::BLACK) {
            json["wrap"] = "black";
        } else if (tex->wrap == Texture::CLAMP) {
            json["wrap"] = "clamp";
        } else {
            error("unknown texture wrapping");
        }

        error_if_not(tex->type != CONSTANT, "invalid texture type");
#ifdef WIN32
        std::string source_path;
        for (int i = 0; i < tex->filename.size(); i++)
            source_path += tex->filename[i] == '/' ? '\\' : tex->filename[i];
        std::string cmd = "copy \"" + source_path + "\" \"" + texture_folder_path + "\"";
        printf("copy command: %s\n", cmd.c_str());
#else
        std::string cmd = "cp \"" + tex->filename + "\" \"" + texture_folder_path + "\"";
#endif
        //printf("copy command: %s\n", cmd.c_str());
        system(cmd.c_str());
        
        std::string filename = tex->filename.substr(tex->filename.find_last_of('/') + 1);
        std::string ext = filename.substr(filename.find_last_of('.'));
        std::string basename = filename.substr(0, filename.find_last_of('.'));
        if (ext == ".exr" || ext == ".tga") {
            json["image_filename"] = texture_folder + basename + ".pfm";
        } else {
            json["image_filename"] = texture_folder + basename + ".ppm";
        }
        return json;
    }

    inline jsonObject procFrame3f( const frame3f &frame ) {
        jsonObject json;
        json["x"] = frame.x;
        json["y"] = frame.y;
        json["z"] = frame.z;
        json["o"] = frame.o;
        return json;
    }

    jsonObject procTransform(const ::Transform *val) {
        jsonObject json;
        if(val->type == IdentityTransform::TYPEID) {
            const IdentityTransform* ptr = (const IdentityTransform*)val;
            json["type"] = "identitytransform";
            json["name"] = ptr->name;
        } else if(val->type == RigidTransform::TYPEID) {
            const RigidTransform* ptr = (const RigidTransform*)val;
            json["type"] = "rigidtransform";
            json["name"] = ptr->name;
            json["frame"] = procFrame3f(ptr->frame);
        } else error("unknown transform type");
        return json;
    }

#define EXPORT_BINARY
    jsonObject procShape(const ::Shape *val) {
        jsonObject json;
        if(val->type == TriangleMeshShape::TYPEID) {
            const TriangleMeshShape* ptr = (const TriangleMeshShape*)val;
            json["type"] = "trianglemeshshape";
            json["name"] = ptr->name;

#ifdef EXPORT_BINARY
            std::string facefile = ptr->name + "_face";
            saveRaw(geometry_folder_path + facefile, ptr->face);
            json["face_filename"] = geometry_folder + facefile;

            std::string posfile = ptr->name + "_position";
            saveRaw(geometry_folder_path + posfile, ptr->pos);
            json["pos_filename"] = geometry_folder + posfile;

            if(!ptr->norm.empty()) {
                std::string normfile = ptr->name + "_normal";
                saveRaw(geometry_folder_path + normfile, ptr->norm);
                json["norm_filename"] = geometry_folder + normfile;
            }

            if(!ptr->uv.empty()) {
                std::string uvfile = ptr->name + "_texcoord";
                saveRaw(geometry_folder_path + uvfile, ptr->uv);
                json["uv_filename"] = geometry_folder + uvfile;
            }
#else
            json["face"] = procVec3iArray(ptr->face);
            json["pos"] = procVec3fArray(ptr->pos);
            if(!ptr->norm.empty()) json["norm"] = procVec3fArray(ptr->norm);
            if(!ptr->uv.empty()) json["uv"] = procVec2fArray(ptr->uv);
#endif
        } else if(val->type == QuadShape::TYPEID) {
            const QuadShape* ptr = (const QuadShape*)val;
            json["type"] = "quadshape";
            json["name"] = ptr->name;
            json["width"] = ptr->width;
            json["height"] = ptr->height;
        } else if(val->type == SphereShape::TYPEID) {
            const SphereShape* ptr = (const SphereShape*)val;
            json["type"] = "sphereshape";
            json["name"] = ptr->name;
            json["radius"] = ptr->radius;
        } else error("unknown shape type");
        return json;
    }

    jsonObject procMaterial(const ::Material *val) {
        jsonObject json;
        json["type"] = "material";
        json["name"] = val->name;
        if(val->opacity) json["opacity_ref"] = val->opacity->name;
        if(val->bump) json["bump_ref"] = val->bump->name;
        if(val->type == LambertEmissionMaterial::TYPEID) {
            const LambertEmissionMaterial* ptr = (const LambertEmissionMaterial*)val;
            json["type"] = "lambertemissionmaterial";
            json["name"] = ptr->name;
            json["le"] = ptr->le;
            if(ptr->leTxt) json["letxt_ref"] = ptr->leTxt->name;
        } else if(val->type == LambertMaterial::TYPEID) {
            const LambertMaterial* ptr = (const LambertMaterial*)val;
            json["type"] = "lambertmaterial";
            json["name"] = ptr->name;
            json["rhod"] = ptr->rhod;
            if(ptr->rhodTxt) json["rhodtxt_ref"] = ptr->rhodTxt->name;
        } else if (val->type == MicrofacetMaterial::TYPEID) {
            const MicrofacetMaterial* ptr = (const MicrofacetMaterial*)val;
            json["type"] = "microfacetmaterial";
            json["name"] = ptr->name;
            json["rhod"] = ptr->rhod;
            if(ptr->rhodTxt) json["rhodtxt_ref"] = ptr->rhodTxt->name;
            json["rhos"] = ptr->rhos;
            if(ptr->rhosTxt) json["rhostxt_ref"] = ptr->rhosTxt->name;
            json["n"] = ptr->n;
            if(ptr->nTxt) json["ntxt_ref"] = ptr->nTxt->name;
            if (ptr->diffuseMode == MDM_LAMBERT) {
                json["diffuse"] = "lambert";
            } else if (ptr->diffuseMode == MDM_ASHIKMINSHIRLEY) {
                json["diffuse"] = "ashikminshirley";
            }
            if (ptr->specularMode == MSM_BLINNPHONG) {
                json["specular"] = "blinnphong";
            } else if (ptr->specularMode == MSM_COOKTORRANCE) {
                json["specular"] = "cooktorrance";
            } else if (ptr->specularMode == MSM_ASHIKMINSHIRLEY) {
                json["specular"] = "ashikmonshirley";
            }
        } else error("unknown material type");
        return json;
    }

    jsonObject procLight(const ::Light *val) {
        jsonObject json;
        if(val->type == PointLight::TYPEID) {
            const PointLight* ptr = (const PointLight*)val;
            json["type"] = "pointlight";
            json["name"] = ptr->name;
            json["intensity"] = ptr->intensity;
            json["transform_ref"] = val->transform->name;
        } else if(val->type == DirectionalLight::TYPEID) {
            const DirectionalLight* ptr = (const DirectionalLight*)val;
            json["type"] = "directionallight";
            json["name"] = ptr->name;
            json["radiance"] = ptr->radiance;
            json["transform_ref"] = val->transform->name;
        } else if(val->type == AreaLight::TYPEID) {
            const AreaLight* ptr = (const AreaLight*)val;
            json["type"] = "arealight";
            json["name"] = ptr->name;
            json["material_ref"] = ptr->material->name;
            json["shape_ref"] = ptr->shape->name;
            json["transform_ref"] = val->transform->name;
        } else if(val->type == EnvironmentLight::TYPEID) {
            const EnvironmentLight* ptr = (const EnvironmentLight*)val;
            json["type"] = "environmentlight";
            json["name"] = ptr->name;
            json["le"] = ptr->le;
            if(ptr->leTxt) json["letxt_ref"] = ptr->leTxt->name;
            json["transform_ref"] = val->transform->name;
        } else error("unknown light type");
        return json;
    }

    jsonObject procCamera(const ::Camera *val) {
        jsonObject json;
        if(val->type == PerspectiveCamera::TYPEID) {
            const PerspectiveCamera* ptr = (const PerspectiveCamera*)val;
            json["type"] = "perspectivecamera";
            json["name"] = ptr->name;
            json["aperture"] = ptr->aperture;
            json["distance"] = ptr->distance;
            json["width"] = ptr->width;
            json["height"] = ptr->height;
            json["transform_ref"] = val->transform->name;
        } else if (val->type == OrthogonalCamera::TYPEID) {
            const OrthogonalCamera* ptr = (const OrthogonalCamera*)val;
            json["type"] = "orthogonalcamera";
            json["name"] = ptr->name;
            json["width"] = ptr->width;
            json["height"] = ptr->height;
            json["transform_ref"] = val->transform->name;
        } else if (val->type == PanoramicCamera::TYPEID) {
            const PanoramicCamera* ptr = (const PanoramicCamera*)val;
            json["type"] = "panoramiccamera";
            json["name"] = ptr->name;
            json["transform_ref"] = val->transform->name;
        } else if (val->type == FisheyeCamera::TYPEID) {
            const FisheyeCamera* ptr = (const FisheyeCamera*)val;
            json["type"] = "fisheyecamera";
            json["name"] = ptr->name;
            json["transform_ref"] = val->transform->name;
        } else error("unknown lens type");
        return json;
    }

    jsonObject procSurface(const ::Surface *val) {
        jsonObject json;
        json["type"] = "surface";
        json["name"] = val->name;
        json["shape_ref"] = val->shape->name;
        json["material_ref"] = val->material->name;
        json["transform_ref"] = val->transform->name;
        return json;
    }
    
    jsonObject procSceneSettings(const std::map<std::string, Surface*> surfaces) {
        jsonObject json;
        json["rayepsilon"] = 0.001;
        return json;
    }

    jsonObject processScene() {
        jsonObject json;
        ExportScene* val = scene;
        json["textures"] = procSceneObjectMap(val->textures, procTexture);
        json["transforms"] = procSceneObjectMap(val->transforms, procTransform);
        json["surfaces"] = procSceneObjectMap(val->surfaces, procSurface);
        json["shapes"] = procSceneObjectMap(val->shapes, procShape);
        json["materials"] = procSceneObjectMap(val->materials, procMaterial);
        json["lights"] = procSceneObjectMap(val->lights, procLight);
        json["cameras"] = procSceneObjectMap(val->cameras, procCamera);
        json["settings"] = procSceneSettings(val->surfaces);
        return json;
    }

    void exportScene() {
        jsonObject json = processScene();
        string ext = getFileExtension(filepath);
        if (ext == "json")
            saveJson(filepath, json);
        else if (ext == "jbin")
            saveBin(filepath, json);
    }

    void cleanup() {
        delete scene;
    }

    template<typename T>
    void insert(std::map<std::string, T*> &m, T* v) {
        typename std::map<std::string, T*>::iterator it = m.find(v->name);
        if (it != m.end()) {
            warning_if_not_va(it->second == v, "name conflict for %s", v->name.c_str());
        }
        m[v->name] = v;
    }
    
    void addMaterial(Material* material) {
        insert(scene->materials, material);
        switch (material->type) {
            case LambertEmissionMaterial::TYPEID: {
                LambertEmissionMaterial *lambert = (LambertEmissionMaterial*)material;
                if (lambert->leTxt) insert(scene->textures, lambert->leTxt);
            } break;
            case LambertMaterial::TYPEID: {
                LambertMaterial *r = (LambertMaterial*)material;
                if (r->rhodTxt) insert(scene->textures, r->rhodTxt);
            } break;
            case MicrofacetMaterial::TYPEID: {
                MicrofacetMaterial *r = (MicrofacetMaterial*)material;
                if (r->rhodTxt) insert(scene->textures, r->rhodTxt);
                if (r->rhosTxt) insert(scene->textures, r->rhosTxt);
                if (r->nTxt) insert(scene->textures, r->nTxt);
            } break;
            default: break;
        }
        
        if (material->bump) insert(scene->textures, material->bump);
        if (material->opacity) insert(scene->textures, material->opacity);
    }

    void addTexture(PbrtTexture* tex){
        insert(scene->textures, (Texture*)tex);
    }

    void addTransform(Transform* transform) {
        insert(scene->transforms, transform);
    }

    void addSurface(Surface* surface) {
        if (surface->transform->type == PbrtTransform::TYPEID) {
            PbrtTransform *xform = static_cast<PbrtTransform*>(surface->transform);
            if (xform->transform.HasScale()) {
                FlattenCopy(surface);
            } else {
                surface->transform = ConvertRigidTransform(xform->transform);
            }
        }
        insert(scene->surfaces, surface);
        insert(scene->shapes, surface->shape);
        addMaterial(surface->material);
        insert(scene->transforms, surface->transform);
    }

    void addInstanceSurface(Surface* surface) {
        if (surface->transform->type == PbrtTransform::TYPEID) {
            PbrtTransform *xform = static_cast<PbrtTransform*>(surface->transform);
            if (xform->transform.HasScale()) {
                FlattenCopy(surface);
            } else {
                surface->transform = ConvertRigidTransform(xform->transform);
            }
            delete xform;
        }
        insert(scene->surfaces, surface);
        insert(scene->shapes, surface->shape);
        addMaterial(surface->material);
        insert(scene->transforms, surface->transform);
    }

    void addLight(Light* light) {
        insert(scene->lights, light);
        if (light->type == EnvironmentLight::TYPEID) {
            EnvironmentLight *env = (EnvironmentLight*)light;
            if(env->leTxt) insert(scene->textures, env->leTxt);
        } else if (light->type == AreaLight::TYPEID) {
            AreaLight *area = (AreaLight*)light;
            if (area->material) addMaterial(area->material);
            insert(scene->shapes, area->shape);
        }
        insert(scene->transforms, light->transform);
    }

    void addCamera( Camera* camera ) {
        insert(scene->cameras, camera);
        insert(scene->transforms, camera->transform);
    }
}