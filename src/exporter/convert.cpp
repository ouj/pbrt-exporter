#include "convert.h"
#include <scene/transform.h>
#include "namer.h"

namespace exporter{

    static IdentityTransform *idXform = 0;

    bool HasIdentityTranform() { return idXform != 0; }
    Transform* GetIdentityTransform() {
        static IdentityTransform idXform;
        idXform.name = "identity_transform";
        error_if_not(idXform.type == IdentityTransform::TYPEID, "identity matrix got changed");
        return &idXform;
    }

    Transform* ConvertCameraTransform(const pbrt::Transform &pbrtXform) {
        RigidTransform *xform = new RigidTransform();
        frame3f &f = xform->frame;
        const pbrt::Matrix4x4 &m = pbrtXform.GetMatrix();
        xform->name = namer::XformName();
        f.x = vec3f(m.m[0][0], m.m[1][0], m.m[2][0]);
        f.y = vec3f(-m.m[0][1], -m.m[1][1], -m.m[2][1]);
        f.z = vec3f(m.m[0][2], m.m[1][2], m.m[2][2]);
        f.o = vec3f(m.m[0][3], m.m[1][3], m.m[2][3]);
        return xform;
    }

    Transform* ConvertRigidTransform(const pbrt::Transform &pbrtXform){
        if (pbrtXform.HasScale()) {
            error("scaling transform not supported! The scene export will not be correct");
            getchar();
            return 0;
        }

        if (pbrtXform.IsIdentity()) {
            return GetIdentityTransform();
        } else {
            RigidTransform *xform = new RigidTransform();
            xform->name = namer::XformName();
            frame3f &f = xform->frame;
            const pbrt::Matrix4x4 &m = pbrtXform.GetMatrix();
            f.x = vec3f(m.m[0][0], m.m[1][0], m.m[2][0]);
            f.y = vec3f(m.m[0][1], m.m[1][1], m.m[2][1]);
            f.z = vec3f(m.m[0][2], m.m[1][2], m.m[2][2]);
            f.o = vec3f(m.m[0][3], m.m[1][3], m.m[2][3]);
            return xform;
        }
    }

    PbrtTransform* ConvertPbrtTransform(const pbrt::Transform & pbrtXform){
        PbrtTransform *xform = new PbrtTransform();
        xform->transform = pbrtXform;
        xform->name = namer::XformName();
        return xform;
    }

    vec3f Convert(const pbrt::Spectrum &s) { 
        float f[3]; 
        s.ToRGB(f); 
        return vec3f(f[0], f[1], f[2]); 
    }

    Transform* Concatenate( const Transform *xform1, const Transform *xform2 ) {
        Transform* ret = 0;
        if (xform1->type == PbrtTransform::TYPEID && xform2->type == PbrtTransform::TYPEID) {
            PbrtTransform *xform = new PbrtTransform();
            xform->transform = static_cast<const PbrtTransform*>(xform1)->transform *
                static_cast<const PbrtTransform*>(xform2)->transform;
            ret = xform;
        } else {
            error("this matrices should not happened");
        }
        return ret;
    }

}