#ifndef TEXTURE_CREATOR_H__
#define TEXTURE_CREATOR_H__
#include <string>
#include <common/vec.h>
#include <scene/scene.h>
#include "transform.h"
#include "mock.h"

namespace exporter{
    PbrtTexture* CreateDefaultFloatTexture(const pbrt::TextureParams &tp, const std::string &tname);
    PbrtTexture* CreateDefaultSpectrumTexture(const pbrt::TextureParams &tp, const std::string &tname);
    PbrtTexture* CreateConstantFloatTexture(const pbrt::TextureParams &tp, const std::string &tname);
    PbrtTexture* CreateConstantSpectrumTexture(const pbrt::TextureParams &tp, const std::string &tname);
    PbrtTexture* CreateImageFloatTexture(const pbrt::TextureParams &tp, const std::string &tname);
    PbrtTexture* CreateImageSpectrumTexture(const pbrt::TextureParams &tp, const std::string &tname);
    PbrtTexture* CreateScaleFloatTexture(const pbrt::TextureParams &tp, const std::string &tname);
    PbrtTexture* CreateScaleSpectrumTexture(const pbrt::TextureParams &tp, const std::string &tname);
}

#endif // TEXTURE_CREATOR_H__