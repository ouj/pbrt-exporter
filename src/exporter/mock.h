
#ifndef _MOCK_H_
#define _MOCK_H_
#include <string>
#include <common/vec.h>
#include <scene/scene.h>

namespace exporter {    
    enum TexType {
        CONSTANT,
        IMAGE,
        BOTH
    };

    struct PbrtTexture : Texture{
        TexType type;
        std::string filename;
        vec3f val;
    };

    PbrtTexture* makeConstTexture(const float val, const std::string &name);
    PbrtTexture* makeConstTexture(const vec3f& val, const std::string &name);

    PbrtTexture* makeImageTexture(const std::string& f, const std::string &name, Texture::WrapMode wrap);

    PbrtTexture* makeCombineTexture(const float val, const std::string& f, const std::string &name, Texture::WrapMode wrap);
    PbrtTexture* makeCombineTexture(const vec3f& val, const std::string& f, const std::string &name, Texture::WrapMode wrap);
}

#endif // _MOCK_H_

