#include "creator_material.h"
#include "scene/reflectance.h"
#include "mock.h"
#include "pbrt.h"
#include "paramset.h"
#include "namer.h"

namespace exporter{
    Material *CreateDefaultMaterial(const pbrt::TextureParams &mp, const char* matname) {
        LambertMaterial *material = new LambertMaterial();
        material->name = namer::ReflectanceName();
        material->rhod = one3f;
        if (matname) material->name = matname;
        else material->name = namer::ReflectanceName();
        return material;
    }

    Material *CreateMatteMaterial(const pbrt::TextureParams &mp, const char* matname) {
        exporter::PbrtTexture* Kd = mp.GetSpectrumTexture("Kd", pbrt::Spectrum(0.5f));
        exporter::PbrtTexture* bumpMap = mp.GetFloatTexture("bumpmap", 0.f);
        LambertMaterial *material = new LambertMaterial();
        if (Kd->type == CONSTANT) {
            material->rhod = Kd->val;
        } else if (Kd->type == IMAGE) {
            material->rhod = one3f;
            material->rhodTxt = Kd;
        } else {
            material->rhod = Kd->val;
            material->rhodTxt = Kd;
        }
        if (matname) material->name = matname;
        else material->name = namer::ReflectanceName();
        if (bumpMap->type == IMAGE) {
            material->bump = bumpMap;
        } else if (bumpMap->type == BOTH) {
            warning("combined texture for bumpmap, only image will be kept.");
            material->bump = bumpMap;
        } else if (bumpMap->type == CONSTANT && bumpMap->val.average() != 0.0f) {
            warning("constant bumpmap not supported.");
        }
        return material;
    }

    Material *CreatePlasticMaterial(const pbrt::TextureParams &mp, const char* matname) {
        exporter::PbrtTexture* Kd = mp.GetSpectrumTexture("Kd", pbrt::Spectrum(0.25f));
        exporter::PbrtTexture* Ks = mp.GetSpectrumTexture("Ks", pbrt::Spectrum(0.25f));
        exporter::PbrtTexture* roughness = mp.GetFloatTexture("roughness", .1f);
        exporter::PbrtTexture* bumpMap = mp.GetFloatTexture("bumpmap", 0.f);
        MicrofacetMaterial* material = new MicrofacetMaterial();
        if (matname) material->name = matname;
        else material->name = namer::ReflectanceName();
        material->diffuseMode = MDM_LAMBERT;
        material->specularMode = MSM_BLINNPHONG;

        if (Kd->type == CONSTANT) {
            material->rhod = Kd->val;
        } else if (Kd->type == IMAGE) {
            material->rhod = one3f;
            material->rhodTxt = Kd;
        } else {
            material->rhod = Kd->val;
            material->rhodTxt = Kd;
        }

        if (Ks->type == CONSTANT) {
            material->rhos = Ks->val;
        } else if (Ks->type == IMAGE) {
            material->rhos = one3f;
            material->rhosTxt = Ks;
        } else {
            material->rhos = Ks->val;
            material->rhodTxt = Ks;
        }

        if (roughness->type == CONSTANT) {
            material->n = roughness->val.average();
        } else if (roughness->type == IMAGE) {
            material->n = 1.0f;
            material->nTxt = roughness;
        } else {
            material->n = roughness->val.average();
            material->nTxt = roughness;
        }

        if (bumpMap->type == IMAGE) {
            material->bump = bumpMap;
        } else if (bumpMap->type == BOTH) {
            warning("combined texture for bumpmap, only image will be kept.");
            material->bump = bumpMap;
        } else if (bumpMap->type == CONSTANT && bumpMap->val.average() != 0.0f) {
            warning("constant bumpmap not supported.");
        }
        return material;
    }
}