#include "creator_camera.h"
#include "paramset.h"
#include "film.h"
#include <scene/camera.h>
#include "namer.h"
#include "convert.h"

namespace exporter {
     Camera* CreatePerspectiveCamera(const pbrt::ParamSet &params, const pbrt::Transform *cam2world, pbrt::Film *film) {
         params.FindOneFloat("shutteropen", 0.f);
         params.FindOneFloat("shutterclose", 1.f);
         float lensradius = params.FindOneFloat("lensradius", 0.f);
         params.FindOneFloat("focaldistance", 1e30f);
         float frame = params.FindOneFloat("frameaspectratio",
             float(film->xResolution)/float(film->yResolution));
         float screen[4];
         if (frame > 1.f) {
             screen[0] = -frame;
             screen[1] =  frame;
             screen[2] = -1.f;
             screen[3] =  1.f;
         }
         else {
             screen[0] = -1.f;
             screen[1] =  1.f;
             screen[2] = -1.f / frame;
             screen[3] =  1.f / frame;
         }
         int swi;
         const float *sw = params.FindFloat("screenwindow", &swi);
         if (sw && swi == 4)
             memcpy(screen, sw, 4*sizeof(float));
         float fov = params.FindOneFloat("fov", 90.);
         float halffov = params.FindOneFloat("halffov", -1.f);
         if (halffov > 0.f)
             // hack for structure synth, which exports half of the full fov
             fov = 2.f * halffov;


         float distance = 1.0;
         float width = distance * tan(pbrt::Radians(fov * 0.5f)) * 4.0f;
         float height = width / frame;

         PerspectiveCamera *camera = new PerspectiveCamera();
         camera->distance = distance;
         camera->width = width;
         camera->height = height;
         camera->aperture = lensradius;
         camera->name = namer::CameraName();
         camera->transform = ConvertCameraTransform(*cam2world);
         return camera;
     }
}