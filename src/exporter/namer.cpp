#include "namer.h"

namespace exporter { namespace namer {
    static int lightId = 0;
    static int sourceId = 0;
    static int xformId = 0;
    static int shapeId = 0;
    static int surfaceId = 0;
    static int materialId = 0;
    static int reflectanceId = 0;
    static int emissionId = 0;
    static int textureId = 0;
    static int cameraId = 0;
    static int lensId = 0;

    void Reset() {
        lightId = sourceId = xformId = 0;
        shapeId = surfaceId = materialId = 0;
        reflectanceId = emissionId = 0;
        textureId = cameraId = lensId = 0;
    }

    inline std::string MakeName(const std::string &type, int &id) {
        char buf[16];
        sprintf(buf, "%d", id);
        id++;
        return type + buf;
    }

    std::string LightName() {
        return MakeName("Light", lightId);
    }

    std::string SourceName() {
        return MakeName("Source", sourceId);
    }

    std::string XformName() {
        return MakeName("Transform", xformId);
    }

    std::string ShapeName() {
        return MakeName("Shape", shapeId);
    }

    std::string SurfaceName() {
        return MakeName("Surface", surfaceId);
    }

    std::string MaterialName() {
        return MakeName("Material", materialId);
    }

    std::string ReflectanceName() {
        return MakeName("Reflectance", reflectanceId);
    }

    std::string EmissionName() {
        return MakeName("Emission", emissionId);
    }

    std::string TextureName() {
        return MakeName("Texture", textureId);
    }

    std::string CameraName() {
        return MakeName("Camera", cameraId);
    }

    std::string LensName() {
        return MakeName("Lens", lensId);
    }

}}