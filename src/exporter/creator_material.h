#ifndef _CREATE_MATERIAL_H_
#define _CREATE_MATERIAL_H_

#include <scene/scene.h>
#include "pbrt.h"

namespace exporter{
    ::Material *CreateDefaultMaterial(const pbrt::TextureParams &mp, const char* matname);
    ::Material *CreateMatteMaterial(const pbrt::TextureParams &mp, const char* matname);
    ::Material *CreatePlasticMaterial(const pbrt::TextureParams &mp, const char* matname);
}

#endif // _CREATE_MATERIAL_H_