#ifndef convert_h__
#define convert_h__
#include "geometry.h"
#include "spectrum.h"
#include "common/vec.h"
#include "transform.h"
#include "scene/transform.h"


namespace exporter {
    struct PbrtTransform : public Transform {
        static const int TYPEID = 99384;
        pbrt::Transform transform;
        PbrtTransform() : Transform(TYPEID) { 
        }
    };
    //convert objects
    bool HasIdentityTranform();
    Transform* GetIdentityTransform();
    Transform* Concatenate(const Transform *xform1, const Transform *xform2);

    Transform* ConvertRigidTransform(const pbrt::Transform & pbrtXform);
    PbrtTransform* ConvertPbrtTransform(const pbrt::Transform & pbrtXform);

    Transform* ConvertCameraTransform(const pbrt::Transform &pbrtXform);
    inline vec3f Convert(const pbrt::Point &p) { return vec3f(p.x, p.y, p.z); }
    inline vec3f Convert(const pbrt::Normal &n) { return normalize(vec3f(n.x, n.y, n.z)); }
    inline vec3f Convert(const pbrt::Vector &v) { return vec3f(v.x, v.y, v.z); }
    vec3f Convert(const pbrt::Spectrum &s);
}


#endif // convert_h__
