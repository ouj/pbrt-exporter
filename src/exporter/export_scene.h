#ifndef EXPORT_SCENE_H__
#define EXPORT_SCENE_H__
#include <vector>
#include <map>
#include <string>
#include <scene/scene.h>
#include <scene/transform.h>
#include <scene/reflectance.h>
#include <scene/emission.h>
#include <scene/shape.h>

namespace exporter{
    struct ExportScene {
        std::map<std::string, Shape*> shapes;
        std::map<std::string, Texture*> textures;
        std::map<std::string, Transform*> transforms;
        std::map<std::string, Material*> materials;
        std::map<std::string, Light*> lights;
        std::map<std::string, Camera*> cameras;
        std::map<std::string, Surface*> surfaces;
    };

    void deleteScene(ExportScene* scene);
}

#endif // EXPORT_SCENE_H__
