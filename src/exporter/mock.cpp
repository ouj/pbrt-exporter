#include "mock.h"

namespace exporter{
    PbrtTexture* makeConstTexture(const float val, const std::string &name) {
        PbrtTexture* tex = new PbrtTexture();
        tex->type = CONSTANT;
        tex->name = name;
        tex->val = vec3f(val, val, val);
        return tex;
    }

    PbrtTexture* makeConstTexture(const vec3f& val, const std::string &name) {
        PbrtTexture* tex = new PbrtTexture();
        tex->type = CONSTANT;
        tex->name = name;
        tex->val = val;
        return tex;
    }

    PbrtTexture* makeImageTexture(const std::string& f, const std::string &name, Texture::WrapMode wrap) { 
        PbrtTexture* tex = new PbrtTexture();
        tex->type = IMAGE;
        tex->filename = f;
        tex->name = name;
        tex->wrap = wrap;
        return tex;
    }


    PbrtTexture* makeCombineTexture(const float val, const std::string& f, const std::string &name, Texture::WrapMode wrap) {
        PbrtTexture* tex = new PbrtTexture();
        tex->type = BOTH;
        tex->val = vec3f(val, val, val);
        tex->filename = f;
        tex->name = name;
        tex->wrap = wrap;
        return tex;
    }

    PbrtTexture* makeCombineTexture(const vec3f& val, const std::string& f, const std::string &name, Texture::WrapMode wrap) {
        PbrtTexture* tex = new PbrtTexture();
        tex->type = BOTH;
        tex->val = val;
        tex->filename = f;
        tex->name = name;
        tex->wrap = wrap;
        return tex;
    }
}

