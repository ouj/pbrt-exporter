#ifndef creator_camera_h__
#define creator_camera_h__
#include "scene/scene.h"
#include "pbrt.h"

namespace exporter{
    Camera* CreatePerspectiveCamera(const pbrt::ParamSet &params, const pbrt::Transform *cam2world, pbrt::Film *film);
}

#endif // creator_camera_h__

