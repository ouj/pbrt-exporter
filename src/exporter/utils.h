#ifndef utils_h__
#define utils_h__
#include "scene/scene.h"

namespace exporter {
    void Flatten(Surface* surface);
    void FlattenCopy(Surface* surface);
}
#endif // utils_h__