#include "creator_light.h"
#include "scene/light.h"
#include "scene/emission.h"
#include "paramset.h"
#include "pbrt.h"
#include "convert.h"
#include "namer.h"

namespace exporter{
    Light *CreatePointLight(const pbrt::Transform &light2world, 
        const pbrt::ParamSet &paramSet) {
            pbrt::Spectrum I = paramSet.FindOneSpectrum("I", pbrt::Spectrum(1.0));
            pbrt::Spectrum sc = paramSet.FindOneSpectrum("scale", pbrt::Spectrum(1.0));
            pbrt::Point P = paramSet.FindOnePoint("from", pbrt::Point(0,0,0));
            pbrt::Transform l2w = pbrt::Translate(pbrt::Vector(P.x, P.y, P.z)) * light2world;

            PointLight *light = new PointLight();
            light->intensity = Convert(I * sc);
            light->transform = ConvertRigidTransform(l2w);
            light->name = namer::LightName();
            return light;
    }

    Light* CreateDistantLight(const pbrt::Transform &light2world, const pbrt::ParamSet &paramSet) {
        pbrt::Spectrum L = paramSet.FindOneSpectrum("L", pbrt::Spectrum(1.0));
        pbrt::Spectrum sc = paramSet.FindOneSpectrum("scale", pbrt::Spectrum(1.0));
        pbrt::Point from = paramSet.FindOnePoint("from", pbrt::Point(0,0,0));
        pbrt::Point to = paramSet.FindOnePoint("to", pbrt::Point(0,0,1));
        UNUSED(from);
        UNUSED(to);
        
        DirectionalLight *light = new DirectionalLight();
        light->radiance = Convert(L * sc);
        light->transform = ConvertRigidTransform(light2world);
        light->name = namer::LightName();
        return light;
    }

    Light* CreateInfiniteLight(const pbrt::Transform &light2world, const pbrt::ParamSet &paramSet) {
        pbrt::Spectrum L = paramSet.FindOneSpectrum("L", pbrt::Spectrum(1.0));
        pbrt::Spectrum sc = paramSet.FindOneSpectrum("scale", pbrt::Spectrum(1.0));
        std::string texmap = paramSet.FindOneString("mapname", "");
        int nSamples = paramSet.FindOneInt("nsamples", 1);
        UNUSED(nSamples);

        EnvironmentLight *light = new EnvironmentLight();
        light->le = Convert(L * sc);
        light->leTxt = makeImageTexture(texmap, namer::TextureName(), Texture::REPEAT);
        light->transform = ConvertRigidTransform(light2world);
        light->name = namer::LightName();

        return light;
    }

    Light* CreateDiffuseAreaLight(const pbrt::Transform &light2world, const pbrt::ParamSet &paramSet, Shape *shape) {
        pbrt::Spectrum L = paramSet.FindOneSpectrum("L", pbrt::Spectrum(1.0));
        pbrt::Spectrum sc = paramSet.FindOneSpectrum("scale", pbrt::Spectrum(1.0));
        int nSamples = paramSet.FindOneInt("nsamples", 1);
        UNUSED(nSamples);

        LambertEmissionMaterial* material = new LambertEmissionMaterial();
        material->name = namer::EmissionName();
        material->le = Convert(L * sc);

        AreaLight *light = new AreaLight();
        light->material = material;
        light->shape = shape;
        light->transform = ConvertRigidTransform(light2world);
        light->name = namer::LightName();

        return light; 
    }
}