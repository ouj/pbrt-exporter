#include "utils.h"
#include "scene/shape.h"
#include "convert.h"
#include "namer.h"

namespace exporter {
    void Flatten(Surface* surface) {
        if (surface->shape->type != TriangleMeshShape::TYPEID) {
            error("only triangle mesh can be flatten");
            return;
        }
        if (surface->transform->type != PbrtTransform::TYPEID) {
            error("only PbrtTransform need to be flatten");
            return;
        }

        PbrtTransform* xform = (PbrtTransform*)surface->transform;
        TriangleMeshShape* shape = (TriangleMeshShape*)surface->shape;
        
        for (int i = 0; i < shape->pos.size(); i++) {
            vec3f& p = shape->pos[i];
            p = Convert(xform->transform(pbrt::Point(p.x, p.y, p.z)));
        }

        for (int i = 0; i < shape->norm.size(); i++) {
            vec3f& n = shape->norm[i];
            n = normalize(Convert(xform->transform(pbrt::Point(n.x, n.y, n.z))));
        }

        surface->transform = GetIdentityTransform();
        error_if_not(surface->transform->type != exporter::PbrtTransform::TYPEID, "not identity matrix");
    }

    void FlattenCopy(Surface* surface) {
        if (surface->shape->type != TriangleMeshShape::TYPEID) {
            error("only triangle mesh can be flatten");
            return;
        }
        if (surface->transform->type != PbrtTransform::TYPEID) {
            error("only PbrtTransform need to be flatten");
            return;
        }

        PbrtTransform* xform = (PbrtTransform*)surface->transform;
        TriangleMeshShape* shape = (TriangleMeshShape*)surface->shape;
        TriangleMeshShape* nshape = new TriangleMeshShape();

        nshape->pos.resize(shape->pos.size());
        nshape->norm.resize(shape->norm.size());

        for (int i = 0; i < shape->pos.size(); i++) {
            vec3f& p = shape->pos[i];
            nshape->pos[i] = Convert(xform->transform(pbrt::Point(p.x, p.y, p.z)));
        }

        for (int i = 0; i < shape->norm.size(); i++) {
            vec3f& n = shape->norm[i];
            nshape->norm[i] = normalize(Convert(xform->transform(pbrt::Normal(n.x, n.y, n.z))));
        }
        
        nshape->face = shape->face;
        nshape->uv = shape->uv;
        nshape->name = namer::ShapeName();
        surface->shape = nshape;
        surface->transform = GetIdentityTransform();
        error_if_not(surface->transform->type != exporter::PbrtTransform::TYPEID, "not identity matrix");
    }
}