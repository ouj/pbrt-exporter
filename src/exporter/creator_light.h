#ifndef _CREATE_LIGHTS_H_
#define _CREATE_LIGHTS_H_
#include "scene/scene.h"
#include "pbrt.h"

namespace exporter{
    Light* CreatePointLight(const pbrt::Transform &light2world, const pbrt::ParamSet &paramSet);
    Light* CreateDistantLight(const pbrt::Transform &light2world, const pbrt::ParamSet &paramSet);
    Light* CreateInfiniteLight(const pbrt::Transform &light2world, const pbrt::ParamSet &paramSet);
    Light* CreateDiffuseAreaLight(const pbrt::Transform &light2world, const pbrt::ParamSet &paramSet, Shape *shape);
}

#endif // _CREATE_LIGHTS_H_