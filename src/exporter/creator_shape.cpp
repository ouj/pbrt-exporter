#include "creator_shape.h"
#include "convert.h"
#include "geometry.h"
#include "paramset.h"
#include "namer.h"

namespace exporter{
    Shape *CreateDiskShape(const pbrt::ParamSet &params, pbrt::Transform &transform) {
            float height = params.FindOneFloat("height", 0.);
            float radius = params.FindOneFloat("radius", 1);
            params.FindOneFloat("innerradius", 0);
            params.FindOneFloat("phimax", 360);

            QuadShape *shape = new QuadShape();
            shape->width = radius * 2.0f;
            shape->height = radius * 2.0f;
            shape->name = namer::ShapeName();
            transform = pbrt::Translate(pbrt::Vector(0, 0, height));
            return shape;
    }

    Shape *CreateSphereShape(const pbrt::ParamSet &params) {
            float radius = params.FindOneFloat("radius", 1.f);
            params.FindOneFloat("zmin", -radius);
            params.FindOneFloat("zmax", radius);
            params.FindOneFloat("phimax", 360.f);
            SphereShape *shape = new SphereShape();
            shape->radius = radius;
            shape->name = namer::ShapeName();
            return shape;
    }

    Shape *CreateTriangleMeshShape(const pbrt::ParamSet &params, bool reverseOrientation) {
            int nvi, npi, nuvi, nsi, nni;
            const int *vi = params.FindInt("indices", &nvi);
            const pbrt::Point *P = params.FindPoint("P", &npi);
            const float *uvs = params.FindFloat("uv", &nuvi);
            if (!uvs) uvs = params.FindFloat("st", &nuvi);
            bool discardDegnerateUVs = params.FindOneBool("discarddegenerateUVs", false);
            // XXX should complain if uvs aren't an array of 2...
            if (uvs) {
                if (nuvi < 2 * npi) {
                    error_va("Not enough of \"uv\"s for triangle mesh.  Expencted %d, "
                        "found %d.  Discarding.", 2*npi, nuvi);
                    uvs = NULL;
                }
                else if (nuvi > 2 * npi)
                    warning_va("More \"uv\"s provided than will be used for triangle "
                    "mesh.  (%d expected, %d found)", 2*npi, nuvi);
            }
            if (!vi || !P) return NULL;
            const pbrt::Vector *S = params.FindVector("S", &nsi);
            if (S && nsi != npi) {
                Error("Number of \"S\"s for triangle mesh must match \"P\"s");
                S = NULL;
            }
            const pbrt::Normal *N = params.FindNormal("N", &nni);
            if (N && nni != npi) {
                Error("Number of \"N\"s for triangle mesh must match \"P\"s");
                N = NULL;
            }
            if (discardDegnerateUVs && uvs && N) {
                // if there are normals, check for bad uv's that
                // give degenerate mappings; discard them if so
                const int *vp = vi;
                for (int i = 0; i < nvi; i += 3, vp += 3) {
                    float area = .5f * Cross(P[vp[0]]-P[vp[1]], P[vp[2]]-P[vp[1]]).Length();
                    if (area < 1e-7) continue; // ignore degenerate tris.
                    if ((uvs[2*vp[0]] == uvs[2*vp[1]] &&
                        uvs[2*vp[0]+1] == uvs[2*vp[1]+1]) ||
                        (uvs[2*vp[1]] == uvs[2*vp[2]] &&
                        uvs[2*vp[1]+1] == uvs[2*vp[2]+1]) ||
                        (uvs[2*vp[2]] == uvs[2*vp[0]] &&
                        uvs[2*vp[2]+1] == uvs[2*vp[0]+1])) {
                            warning("Degenerate uv coordinates in triangle mesh.  Discarding all uvs.");
                            uvs = NULL;
                            break;
                    }
                }
            }
            for (int i = 0; i < nvi; ++i) {
                if (vi[i] >= npi) {
                    error_va("triangle mesh has out of-bounds vertex index %d (%d \"P\" values were given",
                        vi[i], npi);
                    return NULL;
                }
            }

            TriangleMeshShape *shape = new TriangleMeshShape();
            int ntris = nvi / 3;
            shape->face.resize(ntris);
            for (int i = 0; i < ntris; i++) {
                if (reverseOrientation)
                    shape->face.at(i) = vec3i(vi[i*3+0], vi[i*3+1], vi[i*3+2]);
                else
                    shape->face.at(i) = vec3i(vi[i*3+2], vi[i*3+1], vi[i*3+0]);
            }
            shape->pos.resize(npi);
            for (int i = 0; i < npi; i++) {
                shape->pos.at(i) = Convert(P[i]);
            }
            if (N) {
                shape->norm.resize(nni);
                for (int i = 0; i < nni; i++) {
                    if (!reverseOrientation) {
                        shape->norm.at(i) = Convert(N[i]);
                    } else {
                        shape->norm.at(i) = -Convert(N[i]);
                    }
                }
            }
            if (uvs) {
                int nuvs = nuvi / 2;
                shape->uv.resize(nuvs);
                for (int i = 0; i < nuvs; i++) {
                    shape->uv.at(i) = vec2f(uvs[i * 2 + 0], 1-uvs[i * 2 + 1]);
                }
            }
            shape->name = namer::ShapeName();
            return shape;
    }
}