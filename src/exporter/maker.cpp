#include "maker.h"
#include <iostream>
#include "geometry.h"
#include <scene/reflectance.h>
#include "exporter/exporter.h"
#include "common/debug.h"
#include "paramset.h"
#include "convert.h"
#include "creator_light.h"
#include "creator_shape.h"
#include "creator_material.h"
#include "creator_texture.h"
#include "creator_camera.h"
#include "mipmap.h"
#include "namer.h"
using std::cout;
using std::endl;

namespace exporter{
    Light* MakeAreaLight(const string &name,
        const pbrt::Transform &light2world, const ParamSet &paramSet,
        Shape *shape) {
        Light *area = NULL;
        if (name == "area" || name == "diffuse") {
            if (shape->type == TriangleMeshShape::TYPEID) {
                // Approximate it using Quad
                TriangleMeshShape *mesh = (TriangleMeshShape*)shape;
                vec3f normal; 
                range3f bbox = invalidrange3f;
                
                for (int i = 0; i < mesh->pos.size(); i++) {
                    bbox.grow(mesh->pos[i]);
                }
                
                if (mesh->norm.size() > 0) {
                    for(int i = 0; i < mesh->norm.size(); i++)
                        normal += mesh->norm[i];
                } else {
                    for(int f = 0; f < mesh->face.size(); f++) {
                        vec3i face = mesh->face[f];
                        vec3f &p0 = mesh->pos[face.x];
                        vec3f &p1 = mesh->pos[face.y];
                        vec3f &p2 = mesh->pos[face.z];
                        vec3f norm = triangleNormal(p0, p1, p2);
                        float area = triangleArea(p0, p1, p2);
                        normal += norm * area;
                    }
                }
                normal = normalize(normal);
                message_va("light normal %s", normal.print().c_str());
                pbrt::Vector Z(0.0f, -1.0f, 0.0f);
                pbrt::Vector dir(normal.x, normal.y, normal.z);
                pbrt::Vector axis = pbrt::Normalize(pbrt::Cross(Z, dir));
                float angle = pbrt::Degrees(acos(pbrt::Dot(Z, dir)));
                pbrt::Transform dirToZ = pbrt::Rotate(angle, axis);
                vec3f c = bbox.center();
                pbrt::Transform xform = light2world * pbrt::Translate(pbrt::Vector(c.x, c.y, c.z)) * dirToZ;
                vec3f size = bbox.extent();
                
                QuadShape *quad = new QuadShape();
                quad->name = shape->name;
                quad->width = quad->height = mean(size);
                area = exporter::CreateDiffuseAreaLight(xform, paramSet, quad);
            } else 
                area = exporter::CreateDiffuseAreaLight(light2world, paramSet, shape);
        } else
            warning_va("Area light \"%s\" unknown.", name.c_str());
        paramSet.ReportUnused();
        return area;
    }

    Light *MakeLight(const string &name,
        const pbrt::Transform &light2world, const pbrt::ParamSet &paramSet) {
            Light *light = NULL;
            if (name == "point")
                light = CreatePointLight(light2world, paramSet);
            else if (name == "spot") {
                warning((name + " light is not supported, use point light instead").c_str());
                light = CreatePointLight(light2world, paramSet);
            }
            else if (name == "goniometric")
                warning((name + " light is not supported").c_str());
            else if (name == "projection")
                warning((name + " light is not supported").c_str());
            else if (name == "distant")
                light = CreateDistantLight(light2world, paramSet);
            else if (name == "infinite" || name == "exinfinite")
                light = CreateInfiniteLight(light2world, paramSet);
            else
                warning_va("Light \"%s\" unknown.", name.c_str());
            paramSet.ReportUnused();
            return light;
    }

    Shape* MakeShape(const string &name, const ParamSet &paramSet, bool reverseOrientation, pbrt::Transform& transform) {
            Shape *s = NULL;

            if (name == "sphere") {
                s = exporter::CreateSphereShape(paramSet);
                if (reverseOrientation) {
                    warning("reverse orientation is not debuged");
                    transform = pbrt::Scale(-1, -1, -1);
                }
            }
            else if (name == "cylinder")
                warning((name + " shape is not supported").c_str());
            else if (name == "disk") {
                warning((name + " shape is not supported, use quad instead").c_str());
                s = exporter::CreateDiskShape(paramSet, transform);
                if (reverseOrientation) {
                    warning("reverse orientation is not debuged");
                    transform = transform * pbrt::Scale(-1, -1, -1);
                }
            }
            else if (name == "cone")
                warning((name + " shape is not supported").c_str());
            else if (name == "paraboloid")
                warning((name + " shape is not supported").c_str());
            else if (name == "hyperboloid")
                warning((name + " shape is not supported").c_str());
            else if (name == "trianglemesh")
                s = exporter::CreateTriangleMeshShape(paramSet, reverseOrientation);
            else if (name == "heightfield")
                warning((name + " shape is not supported").c_str());
            else if (name == "loopsubdiv")
                warning((name + " shape is not supported").c_str());
            else if (name == "nurbs")
                warning((name + " shape is not supported").c_str());
            else
                warning_va("Shape \"%s\" unknown.", name.c_str());
            //paramSet.ReportUnused();
            return s;
    }


    Material* MakeMaterial(const string &name, const TextureParams &mp, const char* matname) {
            Material *material = NULL;
            if (name == "matte")
                material = exporter::CreateMatteMaterial(mp, matname);
            else if (name == "plastic")
                material = exporter::CreatePlasticMaterial(mp, matname);
            else if (name == "translucent") {
                warning((name + " material is not supported, use default material").c_str());
                material = exporter::CreateDefaultMaterial(mp, matname);
            }
            else if (name == "glass") {
                warning((name + " material is not supported, use default material").c_str());
                material = exporter::CreateDefaultMaterial(mp, matname);
            }
            else if (name == "mirror"){
                warning((name + " material is not supported, use default material").c_str());
                material = exporter::CreateDefaultMaterial(mp, matname);
            }
            else if (name == "mix") {
                warning((name + " material is not supported, use default material").c_str());
                material = exporter::CreateDefaultMaterial(mp, matname);
            }
            else if (name == "metal") {
                warning_va((name + " material is not supported, use plastic instead").c_str());
                material = exporter::CreatePlasticMaterial(mp, matname);
            }
            else if (name == "substrate"){
                warning((name + " material is not supported, use matte material").c_str());
                material = exporter::CreateMatteMaterial(mp, matname);
            }
            else if (name == "uber") {
                warning_va((name + " material is not supported, use plastic instead").c_str());
                material = exporter::CreatePlasticMaterial(mp, matname);
            }
            else if (name == "subsurface") {
                warning((name + " material is not supported, use default material").c_str());
                material = exporter::CreateMatteMaterial(mp, matname);
            }
            else if (name == "kdsubsurface"){
                warning((name + " material is not supported, use default material").c_str());
                material = exporter::CreateMatteMaterial(mp, matname);
            }
            else if (name == "measured"){
                warning((name + " material is not supported, use default material").c_str());
                material = exporter::CreateDefaultMaterial(mp, matname);
            }
            else if (name == "shinymetal") {
                warning_va((name + " material is not supported, use plastic instead").c_str());
                material = exporter::CreatePlasticMaterial(mp, matname);
            }
            else
                warning_va("Material \"%s\" unknown.", name.c_str());
            //mp.ReportUnused();
            if (!material) Error("Unable to create material \"%s\"", name.c_str());
            return material;
    }


    PbrtTexture* MakeFloatTexture(const string &name, const TextureParams &tp, const string &tname) {
        PbrtTexture *tex = NULL;
        if (name == "constant") {
            tex = CreateConstantFloatTexture(tp, tname);
        }
        else if (name == "scale") {
            tex = CreateScaleFloatTexture(tp, tname);
        }
        else if (name == "mix") {
            warning((name + " texture is not supported, use the one of them").c_str());
            tex = tp.GetFloatTexture("tex2", 1.f);
        }
        else if (name == "bilerp") {
            warning_va((name + " texture is not supported, use default texture").c_str());
            tex = CreateDefaultFloatTexture(tp, tname);
        }
        else if (name == "imagemap") {
            tex = CreateImageFloatTexture(tp, tname);
        }
        else if (name == "uv"){
            warning_va((name + " texture is not supported, use default texture").c_str());
            tex = CreateDefaultFloatTexture(tp, tname);
        }
        else if (name == "checkerboard"){
            warning_va((name + " texture is not supported, use default texture").c_str());
            tex = CreateDefaultFloatTexture(tp, tname);
        }
        else if (name == "dots"){
            warning_va((name + " texture is not supported, use default texture").c_str());
            tex = CreateDefaultFloatTexture(tp, tname);
        }
        else if (name == "fbm"){
            warning_va((name + " texture is not supported, use default texture").c_str());
            tex = CreateDefaultFloatTexture(tp, tname);
        }
        else if (name == "wrinkled"){
            warning_va((name + " texture is not supported, use default texture").c_str());
            tex = CreateDefaultFloatTexture(tp, tname);
        }
        else if (name == "marble"){
            warning_va((name + " texture is not supported, use default texture").c_str());
            tex = CreateDefaultFloatTexture(tp, tname);
        }
        else if (name == "windy"){
            warning_va((name + " texture is not supported, use default texture").c_str());
            tex = CreateDefaultFloatTexture(tp, tname);
        }
        else
            warning_va("Float texture \"%s\" unknown.", name.c_str());
        //tp.ReportUnused();
        return tex;
    }


    PbrtTexture* MakeSpectrumTexture(const string &name, const TextureParams &tp, const string &tname) {
            exporter::PbrtTexture *tex = NULL;
            if (name == "constant") {
                tex = CreateConstantSpectrumTexture(tp, tname);
            }
            else if (name == "scale") {
                tex = CreateScaleSpectrumTexture(tp, tname);
            }
            else if (name == "mix"){
                warning((name + " texture is not supported, use the one of them").c_str());
                tex = tp.GetSpectrumTexture("tex2", 1.f);
            }
            else if (name == "bilerp"){
                warning_va((name + " texture is not supported, use default texture").c_str());
                tex = CreateDefaultFloatTexture(tp, tname);
            }
            else if (name == "imagemap") {
                tex = CreateImageSpectrumTexture(tp, tname);
            }
            else if (name == "uv"){
                warning_va((name + " texture is not supported, use default texture").c_str());
                tex = CreateDefaultFloatTexture(tp, tname);
            }
            else if (name == "checkerboard"){
                warning_va((name + " texture is not supported, use default texture").c_str());
                tex = CreateDefaultFloatTexture(tp, tname);
            }
            else if (name == "dots"){
                warning_va((name + " texture is not supported, use default texture").c_str());
                tex = CreateDefaultFloatTexture(tp, tname);
            }
            else if (name == "fbm"){
                warning_va((name + " texture is not supported, use default texture").c_str());
                tex = CreateDefaultFloatTexture(tp, tname);
            }
            else if (name == "wrinkled"){
                warning_va((name + " texture is not supported, use default texture").c_str());
                tex = CreateDefaultFloatTexture(tp, tname);
            }
            else if (name == "marble"){
                warning_va((name + " texture is not supported, use default texture").c_str());
                tex = CreateDefaultFloatTexture(tp, tname);
            }
            else if (name == "windy"){
                warning_va((name + " texture is not supported, use default texture").c_str());
                tex = CreateDefaultFloatTexture(tp, tname);
            }
            else
                warning_va("spectrum texture \"%s\" unknown.", name.c_str());
            //tp.ReportUnused();
            return tex;
    }

    Camera* MakeCamera(const string &name, const pbrt::ParamSet &paramSet, const pbrt::Transform *cam2world, pbrt::Film *film) {
        Camera *camera = 0;
        if (name == "perspective")
            camera = CreatePerspectiveCamera(paramSet, cam2world, film);
        else if (name == "orthographic")
            warning((name + " camera is not supported").c_str());
        else if (name == "environment")
            warning((name + " camera is not supported").c_str());
        else
            Warning("Camera \"%s\" unknown.", name.c_str());
        paramSet.ReportUnused();
        return camera;
    }

}