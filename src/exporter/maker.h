#ifndef _MAKER_H_
#define _MAKER_H_

#include <scene/scene.h>
#include <scene/sceneio.h>
#include <scene/shape.h>
#include <scene.h>
#include "paramset.h"

using pbrt::ParamSet;
using pbrt::TextureParams;
using pbrt::Spectrum;

namespace exporter {    
    Light* MakeAreaLight(const string &name, const pbrt::Transform &light2world, const ParamSet &paramSet, Shape *shape);
    Light* MakeLight(const string &name, const pbrt::Transform &light2world, const pbrt::ParamSet &paramSet);
    Shape* MakeShape(const string &name, const ParamSet &paramSet, bool reverseOrientation, pbrt::Transform &transform);
    Material* MakeMaterial(const string &name, const TextureParams &mp, const char* matname = 0);
    PbrtTexture* MakeFloatTexture(const string &name, const TextureParams &tp, const std::string &tname);
    PbrtTexture* MakeSpectrumTexture(const string &name, const TextureParams &tp, const std::string &tname);
    Camera* MakeCamera(const string &name, const pbrt::ParamSet &paramSet, const pbrt::Transform *cam2world, pbrt::Film *film);
};

#endif //_MAKER_H_