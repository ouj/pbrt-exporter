#include "creator_texture.h"
#include "pbrt.h"
#include "paramset.h"
#include "convert.h"
#include "namer.h"

namespace exporter{
    PbrtTexture* CreateDefaultFloatTexture(const pbrt::TextureParams &tp, const std::string &tname) {
        return makeConstTexture(1.f, tname);
    }

    PbrtTexture* CreateDefaultSpectrumTexture(const pbrt::TextureParams &tp, const std::string &tname) {
        return makeConstTexture(one3f, tname);
    }

    PbrtTexture* CreateConstantFloatTexture(const pbrt::TextureParams &tp, const std::string &tname) {
        return makeConstTexture(tp.FindFloat("value", 1.f), tname);
    }

    PbrtTexture* CreateConstantSpectrumTexture(const pbrt::TextureParams &tp, const std::string &tname) {
        return makeConstTexture(Convert(tp.FindSpectrum("value", pbrt::Spectrum(1.f))), tname);
    }

    PbrtTexture* CreateImageFloatTexture(const pbrt::TextureParams &tp, const std::string &tname) {
        string wrap = tp.FindString("wrap", "repeat");
        tp.FindFloat("scale", 1.f);
        tp.FindFloat("gamma", 1.f);
        Texture::WrapMode w = Texture::REPEAT;
        if (wrap == "black") w = Texture::BLACK;
        else if (wrap == "clamp") w = Texture::CLAMP;
        else if (wrap == "repeat") w = Texture::REPEAT;
        PbrtTexture *tex = makeImageTexture(tp.FindString("filename"), tname, w);
        return tex;
    }
    PbrtTexture* CreateImageSpectrumTexture(const pbrt::TextureParams &tp, const std::string &tname) {
        // Initialize _ImageTexture_ parameters
        string wrap = tp.FindString("wrap", "repeat");
        tp.FindFloat("scale", 1.f);
        tp.FindFloat("gamma", 1.f);
        Texture::WrapMode w = Texture::REPEAT;
        if (wrap == "black") w = Texture::BLACK;
        else if (wrap == "clamp") w = Texture::CLAMP;
        else if (wrap == "repeat") w = Texture::REPEAT;
        PbrtTexture *tex = makeImageTexture(tp.FindString("filename"), tname, w);
        return tex;
    }
    PbrtTexture* CreateScaleFloatTexture(const pbrt::TextureParams &tp, const std::string &tname) {
        PbrtTexture *tex1 = tp.GetFloatTexture("tex1", 1.f);
        PbrtTexture *tex2 = tp.GetFloatTexture("tex2", 1.f);
        if (tex1->type == IMAGE && tex2->type == IMAGE) {
            error("scale image for two image is not supported");
            return tex1;
        } else if (tex1->type == CONSTANT && tex2->type == IMAGE) {
            return makeCombineTexture(tex1->val, tex2->filename, tname, tex2->wrap);
        } else if (tex1->type == IMAGE && tex2->type == CONSTANT) {
            return makeCombineTexture(tex2->val, tex1->filename, tname, tex1->wrap);
        } else if (tex1->type == CONSTANT && tex2->type == CONSTANT) {
            return makeConstTexture(tex1->val * tex2->val, tname);
        } else {
            error("unknown texture type");
            return tex1;
        }
    }
    PbrtTexture* CreateScaleSpectrumTexture(const pbrt::TextureParams &tp, const std::string &tname) {
        PbrtTexture *tex1 = tp.GetSpectrumTexture("tex1", pbrt::Spectrum(1.f));
        PbrtTexture *tex2 = tp.GetSpectrumTexture("tex2", pbrt::Spectrum(1.f));
        if (tex1->type == IMAGE && tex2->type == IMAGE) {
            error("scale image for two image is not supported");
            return tex1;
        } else if (tex1->type == CONSTANT && tex2->type == IMAGE) {
            return makeCombineTexture(tex1->val, tex2->filename, tex2->name, tex2->wrap);
        } else if (tex1->type == IMAGE && tex2->type == CONSTANT) {
            return makeCombineTexture(tex2->val, tex1->filename, tex1->name, tex1->wrap);
        } else if (tex1->type == CONSTANT && tex2->type == CONSTANT) {
            return makeConstTexture(tex1->val * tex2->val, tname);
        } else {
            error("unknown texture type");
            return tex1;
        }
    }
}
