#include "export_scene.h"
#include <map>
#include <string>

namespace exporter{
    template<typename T>
    void _deleteTypeArray(std::map<std::string, T*> a)
    {
        typename std::map<std::string, T*>::const_iterator it;
        for (it = a.begin(); it != a.end(); it++){
            T* p = it->second;
            switch(p->type) {
            case TriangleMeshShape::TYPEID: delete (TriangleMeshShape*)p; break;
            case SphereShape::TYPEID: delete (SphereShape*)p; break;
            case RigidTransform::TYPEID: delete (RigidTransform*)p; break;
            default: delete p;
            }
        }
        a.clear();
    }

    template<typename T>
    void _deleteArray(std::map<std::string, T*> a)
    {
        typename std::map<std::string, T*>::const_iterator it;
        for (it = a.begin(); it != a.end(); it++) delete it->second;
        a.clear();
    }

    void deleteScene( ExportScene *&s )
    {
        if(s == 0) return;
        _deleteTypeArray(s->shapes);
        _deleteTypeArray(s->transforms);
        _deleteArray(s->textures);
        _deleteArray(s->materials);
        _deleteArray(s->lights);
        _deleteArray(s->cameras);
        _deleteArray(s->surfaces);
        delete s;
        s = 0;
    }


    void deleteScene(ExportScene* scene) {
        delete scene;
    }
}