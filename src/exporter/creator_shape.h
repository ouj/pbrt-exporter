#ifndef _CREATE_SHAPES_H_
#define _CREATE_SHAPES_H_

#include <scene/scene.h>
#include <scene/shape.h>
#include "paramset.h"

namespace exporter{
    //create shapes
    Shape *CreateSphereShape(const pbrt::ParamSet &params);
    Shape *CreateTriangleMeshShape(const pbrt::ParamSet &params, bool reverseOrientation);
    Shape *CreateDiskShape(const pbrt::ParamSet &params, pbrt::Transform &transform);
}

#endif // _CREATE_SHAPES_H_