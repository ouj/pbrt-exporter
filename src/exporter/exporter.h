#ifndef _CONVERTER_H_
#define _CONVERTER_H_

#include <scene/scene.h>
#include <scene.h>
#include "paramset.h"
#include "mock.h"

namespace exporter {
    void init(const string &outfile, const string &folder);
    void exportScene();
    void cleanup();
    void addMaterial(Material* material);
    void addTransform(Transform* transform);
    void addSurface(Surface* surface);
    void addInstanceSurface(Surface* surface);
    void addLight(Light* light);
    void addCamera(Camera* camera);
    void addTexture(PbrtTexture* tex);
};

#endif //_CONVERTER_H_