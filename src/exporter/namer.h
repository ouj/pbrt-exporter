#ifndef _NAMER_H_
#define _NAMER_H_
#include <string>

namespace exporter { namespace namer {
    void Reset();

    std::string LightName();
    std::string SourceName();
    std::string XformName();
    std::string ShapeName();
    std::string SurfaceName();
    std::string MaterialName();
    std::string ReflectanceName();
    std::string EmissionName();
    std::string TextureName();
    std::string CameraName();
    std::string LensName();
}}

#endif // _NAMER_H_

